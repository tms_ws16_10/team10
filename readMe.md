# README #
# BadeApp #
![Screenshot_20161021-172530.png](https://bitbucket.org/repo/jpjApr/images/1837600424-Screenshot_20161021-172530.png)
**Beschreibung**
-------------
Die Applikation dient zum Finden von Badestellen in Berlin.
Das Suchspektrum wird aufgeteilt in die jeweiligen Bezirke der Badestellen.
Neben den Standortbezeichnung wie Name der Badestelle und Bezirk ist die Applikation außerdem in der Lage aktuelle Standorttemperaturen auszugeben.
Die Ausgabe der Temperatur erfolgt in der Maßeinheit Celsius [C°] und wird von einem öffentlich Zugänglichen "Open Data"-Server zur Verfügung gestellt.

**Systemvorraussetungen**
----------------------
Android „Marshmallow“ (API 23)

**Funktionen**
----------
Ganz easy!!
Führen Sie die App aus und erhalten Sie mit einem Blick alle "relevanten" Badestellen in Berlin.
Ergänzend zu den Standorten erhalten sie zzgl. die derzeitige Standort Temperatur.
Eine Sortierung in Bezirken ist selbstverständlich kein Problem.

**Verbesserung**
----------------
Die App erfüllt im Allgemein Komplett die Bedürfnisse eines Users der Badestellen finden möchte.
Gut ist auch eine Temperatur anzugeben.
Auch gut wäre es, wenn man ein Hilfe-Menü hätte und das Home-Screen hätte man noch etwas mehr gestalten können.

**Authoren**
------------
Studenten aus dem SS16, TechMobSys, Team04